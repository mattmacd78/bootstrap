# .bash_profile

# Get the aliases and functions
if [ -f ~/.bashrc ]; then
        . ~/.bashrc
fi

# User specific environment and startup programs

PATH=$PATH:$HOME/bin

export PATH

__instances-list()
{
    gcloud compute instances list \
        --format=json \
        --filter "status=RUNNING AND name~${1:-'.*'}"
}

_instances-list()
{
    __instances-list ${1} | \
        jq -cr '.[]|
            {
                "name"   : .name ,
                "ip"     : .networkInterfaces[0].networkIP ,
                "tags"   : [.tags.items[]] ,
                "status" : .status
            }
        '
}

bootstrap()
{
    curl -Lkso- https://gitlab.com/snippets/1911831/raw | bash -x
}


_netstat() { netstat -ntlp ; }

_killall-bash() { pgrep -f bash|grep -v ${$}|xargs kill 2>/dev/null ; }

_redis-cluster-state() { redis-cli -c cluster info|grep '^cluster_state:ok' ; }

es-cluster-status()
{
    curl -s 127.0.0.1:9200/_cat/health?format=json | \
    jq -crM " .[].status " | \
    grep -q ^${1:-green}$ && echo OK >&2 || echo ERROR >&2
}

es-indices-with-health()
{
    es-cluster-status && \
    curl -s 127.0.0.1:9200/_cat/indices?format=json | \
    jq -crM " .[] | select( .health == \"${1:-green}\" ) | .index "
}

alias redis-cli="redis-cli --raw -c"
alias curl="curl -Lks -H'Content-Type:application/json' "
alias tree="tree -C"
alias ls="ls --color=auto"
alias ll="ls -l"
alias l="ls"
alias diff="colordiff"
alias netstat="netstat -ntlp"

unalias rm cp mv 2>/dev/null

export elasticsearch=127.0.0.1:9200
export kibana=127.0.0.1:5601
export nginx=127.0.0.1

passphrase() { diceware -d- --no-caps -n2 ; }

timestamp()
{
    date -Ins
}
payload()
{
    cat << EOF
    {
        "@timestamp" : "$(timestamp)" ,
        "message"    : "$RANDOM"
    }
EOF
}
cmdb-del() { curl -X DELETE  $elasticsearch/${1}?pretty ; }
cmdb-put() { curl -X PUT $elasticsearch/cmdb/env/${1}?pretty -d@${2} ; }
cmdb-get() { curl -X GET "$elasticsearch/cmdb/env/_search?size=10000&pretty" ; }

gpg-reset()
{
    rm -frv $HOME/.gnupg
}

gpg-defaults()
{
    NAME=${1:-username@domain.tld}
}

gpg-list-keys()
{
    NAME=${1:-.*}
    gpg -qa --list-keys ${NAME}
}

gpg-export-keys()
{
    gpg-defaults $1
    gpg -qa --no-emit-version --comment "${NAME}" --export ${NAME}
    gpg -qa --no-emit-version --comment "${NAME}" --export-secret-keys ${NAME}
}

gpg-template()
{
    cat >foo <<EOF
    Key-Type: RSA
    Key-Length: 2048
    Name-Real: ${NAME}
    Expire-Date: 1d
    %commit
EOF
}

gpg-create-keys()
{
    gpg-defaults $1
    gpg-template
    gpg -qa --batch --gen-key foo
    rm -fr foo
}

gpg-keygen()
{
    gpg-defaults $1
    if gpg-list-keys $1
    then
        echo "> found key for $1" 1>&2
        gpg-export-keys $1
    else
        echo "> created key for $1" 1>&2
        gpg-create-keys $1
    fi
}

cmdb-read-source()
{
    $DEBUG && echo $FUNCNAME ${@} 1>&2
    facter -j | jq . > json
}

cmdb-read-redis()
{
    $DEBUG && echo $FUNCNAME ${@} 1>&2
    redis-cli get facter | grep --color=never [[:print:]]
}

cmdb-write-redis()
{
    $DEBUG && echo $FUNCNAME ${@} 1>&2
    cat json | redis-cli -x setex facter 3 | grep -v ^OK$ \
    && return 1 || return 0
}

cmdb-write-es()
{
    $DEBUG && echo $FUNCNAME ${@} 1>&2
    curl -X PUT $elasticsearch/cmdb/env/${1}?pretty -d@${2} 1>&2
}

cmdb-read-es()
{
    $DEBUG && echo $FUNCNAME ${@} 1>&2
    curl -X GET "$elasticsearch/cmdb/env/_search?size=10000&pretty" | \
    jq " .hits.hits | first | ._source "
}

cmdb-query()
{
    $DEBUG && echo $FUNCNAME ${@} 1>&2
    if ! cmdb-read-redis
    then
        cmdb-read-es
        cmdb-read-source
        cmdb-write-redis
        cmdb-write-es dev json
      # cmdb-read-redis
    fi
}
