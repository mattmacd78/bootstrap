#!/bin/bash

systemctl stop postfix
cd files && cp -rvf * /
cd ..
cat /opt/pubkeys/key?? >> $HOME/.ssh/authorized_keys
setenforce permissive
systemctl restart sshd
yum install -y epel-release
PACKAGES="jq nano htop tree wget curl zip unzip nmap-ncat httpd-tools psmisc bind-utils lsof mlocate entr inotify-tools screen git sysstat dnsmasq dnsmasq-utils colordiff python-pip bash-completion bash-completion-extras"
yum install -y ${PACKAGES}
curl https://raw.githubusercontent.com/scopatz/nanorc/v2.9/install.sh | sh
PACKAGES="ansible hiera facter python3-pip fail2ban"
yum install -y ${PACKAGES}
cd files && cp -rvf * /
cd ..
cat /opt/pubkeys/key?? >> $HOME/.ssh/authorized_keys
pip install diceware
TMP=$(mktemp -d)
gsutil 2>/dev/null -m -q rsync -x .git -r gs://poc-infra-v2/ $TMP
tree $TMP
cd $TMP
cp -fr files/opt/letsencrypt.tgz /etc/
cd /etc/
tar zxf letsencrypt.tgz
tree /etc/letsencrypt/
rm -fr /etc/letsencrypt.tgz
systemctl restart nginx
systemctl status nginx
cd $TMP
rm -fr $TMP
curl 127.0.0.1 -Lkvs 2>&1|grep -A5 '^* Server certificate:'

bash /opt/setup-nginx
echo " > Running Time : $SECONDS"
